const passport= require("passport");
const LocalStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuario')

/**estrategia */
passport.use('local',new LocalStrategy(
    function(username, password, done) {
        console.log('entro ')
        console.log(username)
        Usuario.findOne({nombre: username}, function (err, user) {
          console.log("usuario encontrado: "+user)
          if (err) { return done(err); }
          if (!user) {return done(null, false, { message: 'Incorrect username.' });}
          if (!(user.validPassword(password))) { return done(null, false, { message: 'Incorrect password.' }) }
          return done(null, user);
        });
      }
));

/**serializamos un usuario */
passport.serializeUser(function(user,cb){
    cb(null,user.id)
})

/**deserializamos al usuario y passamos el id del usuario a deserializar */
passport.deserializeUser(function(id,cb){
    Usuario.findById(id,function(err,usuario){
        cb(err,usuario)
    })
})

module.exports =passport;