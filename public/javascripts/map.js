//asiganando una ubicacion donde aparecrea la vista del mapa
var map = L.map('map_id').setView([-12.0630149, -77.0296179], 13);

//CREANDO EL TILE DEL TIPO DE MAPA (openstreetmap)
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

//AGREGANDO 3 NODOS
/*
L.marker([-12.0630149, -77.0296179]).addTo(map)
L.marker([-12.0650145, -77.0296179]).addTo(map)  
L.marker([-12.0600140, -77.0296179]).addTo(map)
*/

//en vez de agregarlos nomas usaremos ajax para pedir los datos a la api. y colocar la ubicacion de las bicicletas en el mapa a partir de la ubicacion que se tiene en la api
//USANDO AJAX PARA PEDIR DATOS AL SERVIDOR
$.ajax({
    datatype: "json",
    url: "/api/bicicletas",  //recuerda ponerle el / 
    success: function (result) {
        console.log(result);
        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
});
   