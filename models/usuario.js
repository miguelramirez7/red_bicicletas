var mongoose = require('mongoose')
var Schema = mongoose.Schema;
var Reserva = require('./reserva')
var bcrypt = require('bcrypt')
var uniqueValidator = require('mongoose-unique-validator')
//-----------------------------------------------------------------------
var crypto = require('crypto')
const Token = require('../models/token')
const mailer = require('../mail/mailer')

const saltRound = 10
const validateEmail = function(email) {
    regexemail = /^[^@]+@[^@]+\.[a-zA-Z]{2,}$/;
    return regexemail.test(email);
}
var usuarioSchema = new Schema({
    nombre: {
        type:  String,
        trim: true, //para borrar por si hay espacios cuando colocoan el nombre
        required: [true,'el nombre es obligatorio']
    },
    email:  {
        type: String,
        trim: true,
        required: [true,'el email es obligatorio'],
        lowercase:true,
        unique: true,
        validate: [validateEmail,'por favor ingrese un email valido'],
        match:[/^[^@]+@[^@]+\.[a-zA-Z]{2,}$/]
    },
    password:{
        type:String,
        required:[true,'el password es obligatorio']
    },
    passwordReset :String,
    passwordResetToken: String,
    verificado:{
        type:   Boolean,
        default: false,
    }

})

/**agregando el plugin para que funcione el unique con un mensaje de retorno */
usuarioSchema.plugin(uniqueValidator,{message:'El {PATH} YA EXISTE CON OTRO USUARIO'})

usuarioSchema.pre('save',function(next){
    if(this.isModified('password')){ 
    this.password = bcrypt.hashSync(this.password, saltRound);
    }
    next();
})

usuarioSchema.methods.validPassword =function(password){
    return bcrypt.compareSync(password, this.password);
}


usuarioSchema.methods.reservar  = function(biciId,desde,hasta,cb) {
    var reserva = new Reserva({usuario: this._id,bicicleta:biciId,desde:desde,hasta:hasta})
    console.log(reserva)
    reserva.save(cb)
}

usuarioSchema.methods.enviar_mail_bienvenida = function (cb) {
    const token = new Token({_userId:this.id,token:crypto.randomBytes(16).toString('hex')})
    const email_destination = this.email
    token.save(function(err) {
        if(err){ return console.log(err.message)}

        const email_options = {
            from: 'iammiguel60@gmail.com',
            to: email_destination,
            subject: 'Verificacion de Cuenta',
            text: 'Hola \n\n'+'Por favor para verificar su cuenta haga click en el siguiente link: \n'+'http://localhost:3000/'+'\/token/confirmation\/'+token.token+'.\n'
        }

        mailer.sendMail(email_options,function(err){
            if(err){ return console.log(err.message)}
            console.log('Se ha enviado un mail de Bienvenida a: '+email_destination+'.')
        })
    })

}

module.exports =mongoose.model('Usuario',usuarioSchema)