var mongoose = require('mongoose')
var Schema = mongoose.Schema

var bicicletaSchema = new Schema({
    code: String,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: {type:'2dsphere', sparse: true}    
    }
})
bicicletaSchema.statics.createInstance = function(code,color,modelo,ubicacion){
    return new this({
        code:code,
        color:color,
        modelo:modelo,
        ubicacion:ubicacion
    })
}

bicicletaSchema.methods.toString = function(){
    return  'code: '+this.code+' |color: '+this.color
}

bicicletaSchema.statics.allBicis = function(cb){
    return  this.find({},cb);
}
bicicletaSchema.statics.add  = function(aBici,cb){
    this.create(aBici,cb)
}
bicicletaSchema.statics.findByCode = function(aCode,cb){
    return this.findOne({code:aCode},cb);
}
bicicletaSchema.statics.removeByCode = function(aCode,cb){
    return this.deleteOne({code:aCode},cb);
}
module.exports = mongoose.model('Bicicleta',bicicletaSchema);

































/**LA PARTE DE ABAJO ES EL MODELO SIN USAR MONGOOSE */
/*//constructor
var Bicicleta =  function(id,color, modelo,ubicacion){
    this.id=id;
    this.color=color;
    this.modelo=modelo;
    this.ubicacion=ubicacion;
}


//metodo toStting
Bicicleta.prototype.toString = function(){
    return  `id: ${this.id} | color: ${this.color} | modelo: ${this.modelo}`;
}

//variable
Bicicleta.allBicis= [];

//creamos metodo .add para pushear objetos al arreglo
Bicicleta.add = function(newbici){
    Bicicleta.allBicis.push(newbici);
}

//agregando objetos
var a  = new Bicicleta(1,'azul','urbana',[-12.0600140, -77.0296179]);
var b  = new Bicicleta(2,'verde','urbana',[-12.0500140, -77.0296179]);

//agregamos los objetos al areglo
Bicicleta.add(a);
Bicicleta.add(b);

//metodo para encontrar para los arreglos [var a = areglo.find(x=>x.val==loqueBusco)]
Bicicleta.findById = function (biciId) {
    var bici = Bicicleta.allBicis.find(x => x.id == biciId);
    if(bici)
        return bici
    else
        throw new Error('no se encontro la bicicleta');
}

//metodo para eliminar
Bicicleta.removeById=function (biciId) {
    for (let i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id==biciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
}

//exportando modulo
module.exports = Bicicleta;

*/


