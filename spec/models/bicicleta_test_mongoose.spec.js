var mongoose = require('mongoose')
var Bicicletas = require('../../models/bicicletas');
var chalk = require('chalk')

describe('Testing Bicicletas', ()=>{
    //antes de empezar las pruebas
    beforeEach((done)=>{
        //antes conectamos
        var mongoose = require('mongoose')
        /*mongoose.set('useCreateIndex', true);*/
        /*var URI = 'mongodb://localhost:27017/testdb_bicicletas'
        mongoose.connect(URI,{useNewUrlParser:true, useUnifiedTopology: true})*/
        mongoose.Promise = global.Promise;
        var db = mongoose.connection
        db.on('error',console.error.bind(console, 'MongoDB connection error'))
        db.openUri('mongodb://localhost:27017/testdb_bicicletas')
        db.once('open', function(){
            console.log(chalk.yellow('DB is connected'))
            done()
        })
        
    });
    //despues de terminar la prueba
    afterEach((done) => {
        //eliminamos la base de datos de prueba
        Bicicletas.deleteMany({},(err,success) => {
            if (err) console.log(err)
        })
        done()
    });
    });
    //testeamos metodo crearinstancia del modelo bicicletas
    describe('Bicicleta.createInstance',()=>{
        it('crear una instancia de bicicletas',()=>{
            var bici = Bicicletas.createInstance(1,"verde","urbana",[-34.6012424,-58.3861497])
            expect(bici.code).toBe(1)
            expect(bici.color).toBe("verde")
            expect(bici.modelo).toBe("urbana")
            expect(bici.ubicacion[0]).toEqual(-34.6012424)
            expect(bici.ubicacion[1]).toEqual(-58.3861497)
        });
    });

    //testeamos metodo allBicis
    describe('Bicicleta.allBicis',() => {
        it('comienza vacia',(done) => {
                Bicicletas.allBicis((err,bicis)=>{
                expect(bicis.length).toBe(0)                          
                done()
                })
                done()
        });
    });  
 
 
    //testeamos metodo add
    describe('Bicicletas.add', () => {
        it('agregar solo una bici',(done)=>{
            var aBici = new Bicicletas({code:1,color:"verde",modelo:"urbana",})
            Bicicletas.add(aBici,(err,newBici)=>{
                if(err){ console.log(err) }
                Bicicletas.allBicis((err,bicis)=>{
                    expect(bicis.length).toEqual(1)
                    expect(bicis[0].code).toEqual(aBici.code)
                    done()
                })
            })
            done()
        })
    })

    //testeamos metodo findByCode
    describe('Bicicleta.findByCode', function() {
        it('debe devolver la bici con code = 1', function(done) {
            Bicicletas.allBicis(function(err, bicis) {
                expect(bicis.length).toEqual(0)

                var aBici = new Bicicleta({code:1,color:'verde',modelo:'urbana'})
                Bicicletas.add(aBici,function(err,newBici){
                    if (err){ console.log(err) }

                    var aBici2 = new Bicicleta({code:2,color:'roja',modelo:'urbana'})
                    Bicicletas.add(aBici2,function(err,newBici2){
                        if (err){ console.log(err) }
                        Bicicletas.findByCode(1,function(err,targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        })
                    })
                })
            })
            done()
        })
    })
    
