var mongoose = require('mongoose')
var Bicicletas = require('../../models/bicicletas');
var chalk = require('chalk')
var Usuario =require('../../models/usuario')
var Reserva = require('../../models/reserva')


describe('Testing Bicicletas', ()=>{
    //antes de empezar las pruebas
    beforeEach((done)=>{
        //antes conectamos
        var mongoose = require('mongoose')
        /*mongoose.set('useCreateIndex', true);*/
        /*var URI = 'mongodb://localhost:27017/testdb_bicicletas'
        mongoose.connect(URI,{useNewUrlParser:true, useUnifiedTopology: true})*/
        mongoose.Promise = global.Promise;
        var db = mongoose.connection
        db.on('error',console.error.bind(console, 'MongoDB connection error'))
        db.openUri('mongodb://localhost:27017/testdb_bicicletas')
        db.once('open', function(){
            console.log(chalk.yellow('DB is connected'))
            done()
        })
        
    });
    //despues de terminar la prueba
    afterEach((done) => {
        //eliminamos la base de datos de prueba
        Bicicletas.deleteMany({},(err,success) => {
            if (err) console.log(err)
        })
        done()
    });
    });

//probando metodo reservar
describe('cuando un usuario reserv una bici',function(){
    it('debe existir la reserva',function(done){
        const usuario = new Usuario({nombre:'Ezequiel'})
        usuario.save()

        const bicicleta = new Bicicletas({code:1,color:'verde',modelo:'urbana'})
        bicicleta.save()

        var hoy = new Date()
        var mañana = new Date()

        mañana.setDate(hoy.getDate()+1)
        usuario.reservar(bicicleta.id,hoy,mañana,function(err,reserva) {
            Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err,reservas){
                console.log(reservas[0])
                expect(reservas.length).toBe(1)
                expect(reservas[0].diasDeReserva()).toBe(2)
                expect(reservas[0].bicicleta.code).toBe(1)
                expect(reservas[0].usuario.nombre).toBe(usuario.nombre)
            })
        })
        done()
    })
})