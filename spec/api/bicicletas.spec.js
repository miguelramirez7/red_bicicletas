/*var Bicicletas = require('../../models/bicicletas');
var request = require('request');
var server = require('../../bin/www');
const { response } = require('../../app');

beforeEach(()=>{Bicicletas.allBicis = []; });

describe('GET bicicletas',()=>{
    it('status 200',()=>{
        expect(Bicicletas.allBicis.length).toBe(0);

        var a = new Bicicletas(1,"negro","urbana",[-34.6012424,-58.3861497]);
        Bicicletas.add(a)

        request.get('http://localhost:3000/',(error,response)=>{
            expect(response.statusCode).toBe(200);
        })
    })
});

describe('POST de bicicletas /create',()=>{
    it('status 200',(done)=>{
        var header= {'content-type': 'application/json'}
        var aBici = '{"id":10,"color":"rojo","modelo":"urbana","lat":-34,"lng":-54}'

        request.post({
            headers: header,
            url: 'http://localhost:3000/api/bicicletas/create',
            body: aBici
        },(error,response,body)=>{
            expect(response.statusCode).toBe(200)
            expect(Bicicletas.findById(10).color).toBe("rojo")
            done()
        })

    })
})

describe('POST bicicletas /delete',()=>{
    it('status 200',(done)=>{
        var header= {'content-type': 'application/json'}
        var aBici = new Bicicletas(10,"verde","montaña",[-34.6012424,-58.3861497]);
        Bicicletas.add(aBici);
        var idBici = '{"id":10}'
        
        expect(Bicicletas.findById(10).color).toBe("verde")
        expect(Bicicletas.allBicis.length).toBe(1)

        request.delete({
            headers: header,
            url: 'http://localhost:3000/api/bicicletas/delete',
            body: idBici
        },(error,response,body)=>{
            expect(response.statusCode).toBe(204)
            expect(Bicicletas.allBicis.length).toBe(0)
            done()
        }) 
    })
})

describe('POST bicicletas /update',()=>{
    it('status 200',(done)=>{
        var header= {'content-type': 'application/json'}
        var aBici = new Bicicletas(10,"verde","montaña",[-34.6012424,-58.3861497]);
        Bicicletas.add(aBici);
        var newBici = '{"id":10,"color":"rojo","modelo":"urbana","lat":-34,"lng":-54}'
        
        expect(Bicicletas.findById(10).color).toBe("verde")
        expect(Bicicletas.allBicis.length).toBe(1)

        request.post({
            headers: header,
            url: 'http://localhost:3000/api/bicicletas/update',
            body: newBici
        },(error,response,body)=>{
            expect(response.statusCode).toBe(200)
            expect(Bicicletas.findById(10).color).toBe("rojo")
            done()
        }) 
    })
})
*/