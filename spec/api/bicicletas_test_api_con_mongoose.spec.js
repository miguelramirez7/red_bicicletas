var mongoose = require('mongoose')
var Bicicletas = require('../../models/bicicletas');
var server = require('../../bin/www')
var request = require('request')

var base_url = 'http://localhost:3000/api/bicicletas'


describe('Bicicleta API con monodb', ()=>{
    //antes de empezar las pruebas
    beforeEach((done)=>{
        //antes conectamos
        var mongoose = require('mongoose')
        /*mongoose.set('useCreateIndex', true);*/
        /*var URI = 'mongodb://localhost:27017/testdb_bicicletas'
        mongoose.connect(URI,{useNewUrlParser:true, useUnifiedTopology: true})*/
        mongoose.Promise = global.Promise;
        var db = mongoose.connection
        db.on('error',console.error.bind(console, 'MongoDB connection error'))
        db.openUri('mongodb://localhost:27017/testdb_bicicletas')
        db.once('open', function(){
            console.log(chalk.yellow('DB is connected'))
            done()
        })
        
    });
    //despues de terminar la prueba
    afterEach((done) => {
        //eliminamos la base de datos de prueba
        Bicicletas.deleteMany({},(err,success) => {
            if (err) console.log(err)
        })
        done()
    });
});

    describe('Get Bicicleta / ',function() {
        it ('status 200',function(done) {
            request.get(base_url,function(err,res,body) {
                var result = JSON.parse(body)
                expect(response.statusCode).to.Be(200)
                expect(result.bicicletas.length).toBe(200)
                done()
            })
        })
    })

    describe('POST Bicicletas  /create ',function() {
        it ('status 200',function(done) {
            
            var  headers = {'content-type':'application/json'}
            var abici = '{"code":10,"color":"rojo","modelo":"urbana"}'
            request.post({
                headers:headers,
                url: base_url+'/create',
                body:abici,
            },function(err,response,body){
                expect(response.statusCode).toBe(200)
                var bici = JSON.parse(body).bicicleta
                console.log(bici)

                expect(bici.color).toBe('rojo')
                expect(bici.ubicacion[0]).toBe(-34)
                expect(bici.ubicacion[1]).toBe(-54)
                done()
            }
            )
            done()
        })
    })