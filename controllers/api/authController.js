var Usuario = require('../../models/usuario')
var bcrypt = require('bcrypt')
var jwt = require('jsonwebtoken')

module.exports = {
    authenticate: function (req,res,next) {
        Usuario.findOne({nombre: req.body.username}, function (err,user){
            if (err){next(err)
            }else{
                if(user==null){return res.status(401).json({status:"error",message:"usuario no encontrado"})}
                if(user!=null && bcrypt.compareSync(req.body.password,user.password)){
                    user.save(function(err,userSave){
                            const token = jwt.sign({id:userSave.id}, req.app.get('secretkey'), {expiresIn : '7d'})
                            res.status(200).json({message: 'usuario encontrado', data: {usuario: userSave, toke: token}})
                    })
                }else{ res.status(401).json({status: 'error!', message: "invalid password!", data: null})}
            }
        })
    },

    forgotpassword: function(req, res,next){
      Usuario.findOne({username: req.body.username}, function(err,user){
          if(!user) return res.status(401).json({message:"no existe el usuario",data:null})
          console.log("resetpasssord metodo falat implementar")    
      })  
    }
}
