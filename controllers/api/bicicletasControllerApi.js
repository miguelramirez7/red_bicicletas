var Bicicleta = require("../../models/bicicletas");

//funciones que se daras en las rutas que le  indiquemos en las rutas
exports.Bicicletas_list = function (req,res) {
    Bicicleta.find(function (err,bicicletas){res.status(200).json({
        bicicletas: bicicletas
    });})
    
}

exports.bicicletas_create = function (req,res) {
    var bici = new Bicicleta(req.body.id,req.body.color,req.body.modelo);
        bici.ubicacion = [req.body.lat,req.body.lat];

        Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicletas_delete  = function(req,res){

    Bicicleta.removeById(req.body.id);
    res.status(204).send();  //para no retornar contenido al hacer esta funcion
}

exports.Bicicletas_update = function (req,res) {
    var biciupdate =  Bicicleta.findById(req.body.id);
    biciupdate.id = req.body.id;
    biciupdate.color = req.body.color;
    biciupdate.modelo = req.body.modelo;
    biciupdate.ubicacion = [req.body.lat,req.body.long];

    res.status(200).json({
        bicicleta: biciupdate
    });
    

}