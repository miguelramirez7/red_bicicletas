var bicicleta = require('../models/bicicletas');
const Bicicleta = require('../models/bicicletas');

//exportando funcion listar bicicletas que renderiza en index y pasa el objeto bicis
exports.bicicleta_list = function(req,res){
    bicicleta.allBicis((err,bicis)=>{
        console.log("saa"+bicis.length)
            
        res.render('bicicletas/index',{ bicis : bicis});
    })
    
}
//exportando funcion  crear_get  que renderiza en create
exports.bicicleta_create_get = function (req,res) {
    res.render('bicicletas/create');
}
//exportando funcion crear_post una vez creado el nuevo objeto redirecciona a /bicicletas
exports.bicicleta_create_post= function (req,res) {
    console.log("datos entran: "+req.body.id,req.body.color,req.body.modelo)
    console.log("----")
    var bici = new bicicleta({code:req.body.id,color:req.body.color,modelo:req.body.modelo})
    bici.ubicacion= [req.body.lat,req.body.long];
    console.log("----")
    console.log("bici: "+bici.code)
    console.log("****")
    bicicleta.add(bici);

    res.redirect('/bicicletas');
}
//exportando funcion eliminar_post  que luego de presionar el boton redirecciona 
exports.bicicleta_delete_post=function (req,res) {
    Bicicleta.removeById(req.params.id);
    res.redirect('/bicicletas');
}
//exportando funcion actualizar_get  que renderiza en update y manda el objeto bicis que tendra los datos de la bicicleta que actualizaremos
exports.bicicleta_update_get =  function (req,res) {
    var bici = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update',{bici})
}
//exportando funcion actualizar_post una vez actualiza el objeto  redirecciona a la lista en /bicicletas
exports.bicicleta_update_post = function(req,res){
    var bici = Bicicleta.findById(req.params.id);

    bici.id =req.body.id;
    bici.color =req.body.color;
    bici.modelo =req.body.modelo;
    bici.ubicacion =[req.body.lat,req.body.long];

    res.redirect('/bicicletas');


}