var Usuario = require('../models/usuario')
var Token = require('../models/token')

module.exports = {
    connfirmationGet: function (req,res,next) {
        Token.findOne({ token:req.params.token},function(err,token){
            if(!token) return res.status(400).send({type:'not-invalidate',msg:'No encontramos un usuario con este token'})
            Usuario.findById(token._userId ,function(err,user){
                if (!user) return res.status(400).send({msg:'No encontramos un usuario con este token'})
                if (user.verificado) return res.redirect('/users') //OJO
                usuario.verificado =true//verificado se pone en true
                usuario.save(function(err){ 
                    if (err) { return res.status(500).send({msg:err.message})}
                    res.redirect('/')
                })
            })
        })
    }
}