require("dotenv").config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var passport = require('passport')
const userpassport = require('./config/passport')
const session = require('express-session')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var biclicletasRouter = require('./routes/bicicletas');
var bicicletasApiRouter = require('./routes/api/bicicletas');
var tokenRouter = require('./routes/token')
var authRouter = require('./routes/api/auth')
//para validar el token
var jwt = require('jsonwebtoken')

var chalk = require('chalk')
///desplegado en heroku

/**para la sesion */
const store = new session.MemoryStore

var app = express();

//creando variable secretkey de valor jwt_pwd_!!1223344
app.set('secretkey','jwt_pwd_!!1223344');
/**configuurando middleware de la session */
app.use(session({
  cookie: { maxAge: 1000* 60 * 60 * 240},//dias dias en milisegundos
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'misecretoparaestasesion_red/*/bici'

}))

//coneccion con mongoose a la base de datos en mongo
var mongoose = require('mongoose')
//falta
//google oauth
//facebook oauth
var URI = process.env.MONGO_URI;

mongoose.set('useCreateIndex', true);
//var URI = 'mongodb://localhost:27017/red_bicicletas'
mongoose.connect(URI,{useNewUrlParser:true,useUnifiedTopology: true }).then(db => console.log(chalk.yellow('DB is connected')) )
mongoose.Promise = global.Promise;
var db = mongoose.connection
db.on('error',console.error.bind(console, 'MongoDB connection error'))



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(userpassport.initialize());
app.use(passport.session());
/*app.use((req,res,next)=>{
  console.log(req.session)
  console.log(req.body)
  next()
})*/

app.use(express.static(path.join(__dirname, 'public')));


/**rutas para la session */
app.get('/login',function(req,res){
  console.log("get login")
  res.render('session/login')
})



app.post('/login',function(req,res,next){
  console.log("usuario:"+req.body.username+" "+req.body.password)
  console.log("post login")
  passport.authenticate('local',function(err,usuario, info){
    console.log("usuario ya pasado: " + req.user)
    console.log("paso logIn")
    console.log(usuario+ " " + err)
    if(err) return next(err)
    if(!usuario) return res.render('session/login',{info:info})
    req.logIn(usuario,function(err){
        if(err){ return next(err) }
        return res.redirect('/');
    })
  })(req,res, next)
})



app.get('/logout',function(req,res){
  req.logout()
  res.redirect('/')
})

app.get('/forgotpassword',function(req,res){
  res.render('session/forgotpassword')
})

//------------------------
app.use('/', indexRouter);
app.use('/users',usersRouter);
app.use('/token', tokenRouter);

//este loggedIn es el middleware para controlar que este logeado para pasar 
app.use('/bicicletas',loggedIn,biclicletasRouter);
app.use('/api/bicicletas',validarUsuario,bicicletasApiRouter);
app.use('/api/auth',authRouter);
app.use(function(req,res){console.log("middleware general")})
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


//funcion para asegurar que el usuario se encuentra logeado
function loggedIn(req,res,next) {
  if(req.user){
    next();
  }else{
    console.log('user sin logearse')
    res.redirect('/login');
  }
}

/*validar usuario como middleware*/
function validarUsuario(req,res,next){
  jwt.verify(req.headers['x-access-token'],req.app.get('secretkey'),function(err,decoded){
    if(err){
      res.json({status:"error", message:err.message,data:null});
    }else{
      req.body.userId = decoded.id;
      console.log("jwt  verify: " + decoded)
      next()
    }
  })
}

module.exports = app;
