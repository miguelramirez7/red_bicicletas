const express = require('express')
const router = express.Router()

/*importando controladores para nuestras rutas*/
const authontroller = require('../../controllers/api/authController')

//definimos las rutas
router.post('/authenticate', function(req, res,next){ console.log('paso autenticacionController'); next()} , authontroller.authenticate)
router.post('/forgotpassword',authontroller.forgotpassword)

module.exports = router;
