var express = require("express");
var router =  express.Router();
var bicicletaController = require("../../controllers/api/bicicletasControllerApi");

//rutas con las fucniones creadas en el controller para la api
router.get("/",bicicletaController.Bicicletas_list);
router.post("/create",bicicletaController.bicicletas_create);
router.delete("/delete",bicicletaController.bicicletas_delete);
router.post("/update",bicicletaController.Bicicletas_update);

module.exports = router;